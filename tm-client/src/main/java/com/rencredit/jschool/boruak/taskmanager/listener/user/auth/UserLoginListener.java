package com.rencredit.jschool.boruak.taskmanager.listener.user.auth;

import com.rencredit.jschool.boruak.taskmanager.endpoint.*;
import com.rencredit.jschool.boruak.taskmanager.event.ConsoleEvent;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptySessionException;
import com.rencredit.jschool.boruak.taskmanager.listener.AbstractListener;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class UserLoginListener extends AbstractListener {

    @Autowired
    private AuthEndpoint authEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "login";
    }

    @NotNull
    @Override
    public String description() {
        return "Login user in program";
    }

    @Override
    @EventListener(condition = "@userLoginListener.name() == #event.command")
    public void handle(final ConsoleEvent event)throws EmptyPasswordException_Exception, EmptyUserException_Exception, EmptyLoginException_Exception, EmptyHashLineException_Exception, DeniedAccessException_Exception, EmptySessionException {
        System.out.println("Enter login");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("Enter password");
        @NotNull final String password = TerminalUtil.nextLine();

        @NotNull final SessionDTO sessionFromServer = authEndpoint.logIn(login, password);
        systemObjectService.putSession(sessionFromServer);
        System.out.println("Login success");
    }

}
