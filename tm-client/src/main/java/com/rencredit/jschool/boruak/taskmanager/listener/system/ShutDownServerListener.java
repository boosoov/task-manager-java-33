package com.rencredit.jschool.boruak.taskmanager.listener.system;

import com.rencredit.jschool.boruak.taskmanager.endpoint.*;
import com.rencredit.jschool.boruak.taskmanager.event.ConsoleEvent;
import com.rencredit.jschool.boruak.taskmanager.listener.AbstractListener;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class ShutDownServerListener extends AbstractListener {

    @Autowired
    private AdminEndpoint adminEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "shut-down-server";
    }

    @NotNull
    @Override
    public String description() {
        return "Shut down server.";
    }

    @Override
    @EventListener(condition = "@shutDownServerListener.name() == #event.command")
    public void handle(final ConsoleEvent event)throws IOException_Exception, NotExistUserException_Exception, EmptyRoleException_Exception, DeniedAccessException_Exception, EmptySessionException_Exception, EmptyUserIdException_Exception, EmptyIdException_Exception {
        @NotNull final SessionDTO session = systemObjectService.getSession();
        adminEndpoint.shutDownServer(session);
    }

}
