package com.rencredit.jschool.boruak.taskmanager.exception.notexist;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractClientException;

public class NotExistUsersListException extends AbstractClientException {

    public NotExistUsersListException() {
        super("Error! Users list is not exist...");
    }

}
