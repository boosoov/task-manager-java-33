package com.rencredit.jschool.boruak.taskmanager.exception.empty;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractClientException;

public class EmptyDescriptionException extends AbstractClientException {

    public EmptyDescriptionException() {
        super("Error! Description is empty...");
    }

}
