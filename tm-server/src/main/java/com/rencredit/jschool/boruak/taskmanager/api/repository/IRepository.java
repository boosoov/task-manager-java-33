package com.rencredit.jschool.boruak.taskmanager.api.repository;

import com.rencredit.jschool.boruak.taskmanager.dto.AbstractEntityDTO;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;

public interface IRepository<E extends AbstractEntityDTO> {

//    void begin();
//
//    void commit();
//
//    void rollback();
//
//    void close();

    void clearAll();

    void add(@NotNull E record);

    void remove(@NotNull E record);

    void load(@NotNull Collection<E> elements);

    void load(@NotNull E... elements);

    void merge(@NotNull E element);

    void merge(@NotNull Collection<E> elements);

    void merge(@NotNull E... elements);

}
