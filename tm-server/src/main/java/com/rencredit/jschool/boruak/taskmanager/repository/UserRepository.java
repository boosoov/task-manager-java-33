package com.rencredit.jschool.boruak.taskmanager.repository;

import com.rencredit.jschool.boruak.taskmanager.api.repository.IUserRepository;
import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class UserRepository extends AbstractRepository<UserDTO> implements IUserRepository {

    @NotNull
    @Override
    public List<UserDTO> getListDTO() {
        return entityManager.createQuery("SELECT e FROM UserDTO e", UserDTO.class).getResultList();
    }

    @NotNull
    @Override
    public List<User> getListEntity() {
        return entityManager.createQuery("SELECT e FROM User e", User.class).getResultList();
    }

    @Nullable
    @Override
    public UserDTO findByIdDTO(@NotNull final String id) {
        List<UserDTO> listUsers = entityManager.createQuery("SELECT e FROM UserDTO e WHERE e.id = :id", UserDTO.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList();
        if (listUsers.isEmpty()) return null;
        return listUsers.get(0);
    }

    @Nullable
    @Override
    public User findByIdEntity(@NotNull final String id) {
        List<User> listUsers = entityManager.createQuery("SELECT e FROM User e WHERE e.id = :id", User.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList();
        if (listUsers.isEmpty()) return null;
        return listUsers.get(0);
    }

    @Nullable
    @Override
    public UserDTO findByLoginDTO(@NotNull final String login) {
        List<UserDTO> listUsers = entityManager.createQuery("SELECT e FROM UserDTO e WHERE e.login = :login", UserDTO.class)
                .setParameter("login", login)
                .setMaxResults(1)
                .getResultList();
        if (listUsers.isEmpty()) return null;
        return listUsers.get(0);
    }

    @Nullable
    @Override
    public User findByLoginEntity(@NotNull final String login) {
        List<User> listUsers = entityManager.createQuery("SELECT e FROM User e WHERE e.login = :login", User.class)
                .setParameter("login", login)
                .setMaxResults(1)
                .getResultList();
        if (listUsers.isEmpty()) return null;
        return listUsers.get(0);
    }

    @Override
    public void removeById(@NotNull final String id) {
        @Nullable final User user = findByIdEntity(id);
        if (user == null) return;
        entityManager.remove(user);
    }

    @Override
    public void removeByLogin(@NotNull final String login) {
        @Nullable final User user = findByLoginEntity(login);
        if (user == null) return;
        entityManager.remove(user);
    }

    @Override
    public void removeByUser(@NotNull final UserDTO user) {
        removeById(user.getId());
    }

    @Override
    public void clearAll() {
        @NotNull final List<User> listUsers = getListEntity();
        for (@NotNull final User user : listUsers) {
            entityManager.remove(user);
        }
    }

    public void addUser(@NotNull final User user) {
        entityManager.persist(user);
    }

}
