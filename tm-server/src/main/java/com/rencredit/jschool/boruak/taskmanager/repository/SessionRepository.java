package com.rencredit.jschool.boruak.taskmanager.repository;

import com.rencredit.jschool.boruak.taskmanager.api.repository.ISessionRepository;
import com.rencredit.jschool.boruak.taskmanager.dto.SessionDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class SessionRepository extends AbstractRepository<SessionDTO> implements ISessionRepository {

    @NotNull
    @Override
    public List<SessionDTO> getListDTO() {
        return entityManager.createQuery("SELECT e FROM SessionDTO e", SessionDTO.class)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Session> getListEntity() {
        return entityManager.createQuery("SELECT e FROM Session e", Session.class)
                .getResultList();
    }

    @Override
    public void removeByUserId(@NotNull final String id) {
        @Nullable final Session session = findById(id);
        if (session == null) return;
        removeBySession(session);
    }

    @Override
    public void removeBySession(@NotNull final Session session) {
        entityManager.remove(session);
    }

    @Nullable
    @Override
    public Session findById(@NotNull final String id) {
        List<Session> listSessions = entityManager.createQuery("SELECT e FROM Session e WHERE e.id = :id", Session.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList();
        if (listSessions.isEmpty()) return null;
        return listSessions.get(0);
    }

    @Override
    public boolean contains(@NotNull final String Id) {
        @Nullable final Session session = findById(Id);
        if (session == null) return false;
        return true;
    }

    @Override
    public void clearAll() {
        @NotNull final List<Session> listSessions = getListEntity();
        for (@NotNull final Session session : listSessions) {
            entityManager.remove(session);
        }
    }

}
