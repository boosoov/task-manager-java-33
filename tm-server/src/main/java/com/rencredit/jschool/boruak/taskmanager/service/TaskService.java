package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.repository.ITaskRepository;
import com.rencredit.jschool.boruak.taskmanager.api.service.ITaskService;
import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectIndexException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.PersistenceContext;
import java.util.List;

@Service
public class TaskService extends AbstractService<TaskDTO> implements ITaskService {

    @Autowired
    private ITaskRepository repository;

    @Override
    @Transactional
    public void create(@Nullable final String userId, @Nullable final String name) throws EmptyNameException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @NotNull final TaskDTO task = new TaskDTO(userId, name);
        repository.add(task);
    }

    @Override
    @Transactional
    public void create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) throws EmptyDescriptionException, EmptyNameException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();

        @NotNull final TaskDTO task = new TaskDTO(userId, name, description);
        repository.add(task);
    }

    @Override
    @Transactional
    public void create(@Nullable final String userId, @Nullable final TaskDTO task) throws EmptyTaskException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (task == null) throw new EmptyTaskException();

        repository.add(task);
    }

    @Override
    @Transactional
    public void remove(@Nullable final String userId, @Nullable final TaskDTO task) throws EmptyTaskException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (task == null) throw new EmptyTaskException();

        repository.remove(task);
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByUserId(@Nullable final String userId) throws EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();

        @NotNull final List<TaskDTO> tasks = repository.findAllByUserIdDTO(userId);
        return tasks;
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(@Nullable final String projectId) throws EmptyProjectIdException {
        if (projectId == null || projectId.isEmpty()) throw new EmptyProjectIdException();

        @NotNull final List<TaskDTO> tasks = repository.findAllDTOByProjectId(projectId);
        return tasks;
    }

    @Override
    @Transactional
    public void clearByUserId(@Nullable final String userId) throws EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();

        repository.clearByUserId(userId);
    }

    @Nullable
    @Override
    public TaskDTO findOneByIndex(@Nullable final String userId, @Nullable final Integer index) throws EmptyUserIdException, IncorrectIndexException {
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();

        @NotNull final TaskDTO task = repository.findOneDTOByIndex(userId, index);
        return task;
    }

    @Nullable
    @Override
    public TaskDTO findOneByName(@Nullable final String userId, @Nullable final String name) throws EmptyUserIdException, EmptyNameException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();


        @Nullable final TaskDTO task = repository.findOneDTOByName(userId, name);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO updateTaskById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws EmptyTaskException, EmptyIdException, EmptyUserIdException, EmptyNameException, EmptyDescriptionException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();

        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new EmptyTaskException();
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        repository.merge(task);
        return task;
    }

    @Override
    @Transactional
    public void attachTaskToProject(
            @Nullable final String userId,
            @Nullable final String taskId,
            @Nullable final String projectId
    ) throws EmptyTaskException, EmptyIdException, EmptyUserIdException, EmptyTaskIdException, EmptyProjectIdException {
        if (taskId == null || taskId.isEmpty()) throw new EmptyTaskIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyProjectIdException();

        @Nullable final TaskDTO task = findOneById(userId, taskId);
        if (task == null) throw new EmptyTaskException();
        task.setProjectId(projectId);
        repository.merge(task);
    }

    @Override
    @Transactional
    public void detachTaskToProject(
            @Nullable final String userId,
            @Nullable final String taskId,
            @Nullable final String projectId
    ) throws EmptyTaskException, EmptyIdException, EmptyUserIdException, EmptyTaskIdException, EmptyProjectIdException {
        if (taskId == null || taskId.isEmpty()) throw new EmptyTaskIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyProjectIdException();

        @Nullable final TaskDTO task = findOneById(userId, taskId);
        if (task == null) throw new EmptyTaskException();
        task.setProjectId(null);
        repository.merge(task);
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO updateTaskByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) throws EmptyTaskException, EmptyNameException, EmptyUserIdException, IncorrectIndexException, EmptyDescriptionException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();

        @Nullable final TaskDTO task = findOneByIndex(userId, index);
        if (task == null) throw new EmptyTaskException();
        task.setName(name);
        task.setDescription(description);
        repository.merge(task);
        return task;
    }

    @Override
    @Transactional
    public void removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) throws IncorrectIndexException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();

        repository.removeOneByIndex(userId, index);
    }

    @Override
    @Transactional
    public void removeOneByName(@Nullable final String userId, @Nullable final String name) throws EmptyNameException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        repository.removeOneByName(userId, name);
    }

    @Nullable
    @Override
    public TaskDTO findOneById(@Nullable final String userId, @Nullable final String id) throws EmptyIdException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        @Nullable final TaskDTO task = repository.findOneDTOById(userId, id);
        return task;
    }

    @Override
    @Transactional
    public void removeOneById(@Nullable final String userId, @Nullable final String id) throws EmptyIdException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        repository.removeOneById(userId, id);
    }

    @NotNull
    @Override
    public List<TaskDTO> getList() {
        @NotNull final List<TaskDTO> tasks = repository.getListDTO();
        return tasks;
    }

    @Override
    @Transactional
    public void clearAll() {
        repository.clearAll();
    }

}
