package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.dto.AbstractEntityDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.notexist.NotExistAbstractListException;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface IService<E extends AbstractEntityDTO> {

    void load(@Nullable Collection<E> elements) throws EmptyElementsException;

    void load(@Nullable E... elements) throws EmptyElementsException;

    boolean merge(@Nullable E element) throws EmptyElementsException;

    void merge(@Nullable Collection<E> elements) throws NotExistAbstractListException;

    void merge(@Nullable E... elements) throws NotExistAbstractListException;

    void clearAll() throws EmptyPasswordException, BusyLoginException, EmptyHashLineException, EmptyLoginException, EmptyUserException, DeniedAccessException, EmptyRoleException;

}
