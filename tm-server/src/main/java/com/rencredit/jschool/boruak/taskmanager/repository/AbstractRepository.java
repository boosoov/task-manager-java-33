package com.rencredit.jschool.boruak.taskmanager.repository;

import com.rencredit.jschool.boruak.taskmanager.api.repository.IRepository;
import com.rencredit.jschool.boruak.taskmanager.dto.AbstractEntityDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;

public class AbstractRepository<E extends AbstractEntityDTO> implements IRepository<E> {

    @Nullable
    @PersistenceContext
    protected EntityManager entityManager;

    public void add(@NotNull final E record) {
        entityManager.persist(record);
    }

    public void remove(@NotNull final E record) {
        entityManager.remove(record);
    }

    public void clearAll() {
        entityManager.clear();
    }

    public void load(@NotNull final Collection<E> records) {
        clearAll();
        merge(records);
    }

    public void load(@NotNull final E... records) {
        clearAll();
        merge(records);
    }

    public void merge(@NotNull final Collection<E> records) {
        for (@NotNull final E record : records) {
            entityManager.merge(record);
        }
    }

    public void merge(@NotNull final E... records) {
        for (@NotNull final E record : records) {
            entityManager.merge(record);
        }
    }

    public void merge(@NotNull final E record) {
        entityManager.merge(record);
    }

}
