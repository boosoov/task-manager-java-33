package com.rencredit.jschool.boruak.taskmanager.api.repository;

import com.rencredit.jschool.boruak.taskmanager.dto.SessionDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ISessionRepository extends IRepository<SessionDTO> {

    List<SessionDTO> getListDTO();

    @NotNull
    List<Session> getListEntity();

    void removeByUserId(@NotNull String Id);

    void removeBySession(@NotNull final Session session);

    @Nullable
    Session findById(@NotNull String id);

    boolean contains(@NotNull String id);

    void clearAll();

}
