package com.rencredit.jschool.boruak.taskmanager.repository;

import com.rencredit.jschool.boruak.taskmanager.api.repository.IProjectRepository;
import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class ProjectRepository extends AbstractRepository<ProjectDTO> implements IProjectRepository {

    @NotNull
    @Override
    public List<ProjectDTO> getListDTO() {
        return entityManager.createQuery("SELECT e FROM ProjectDTO e", ProjectDTO.class)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Project> getListEntity() {
        return entityManager.createQuery("SELECT e FROM Project e", Project.class)
                .getResultList();
    }

    public void addProjectEntity(@NotNull final Project project) {
        entityManager.persist(project);
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllByUserIdDTO(@NotNull final String userId) {
        List<ProjectDTO> listProjects = entityManager.createQuery("SELECT e FROM ProjectDTO e WHERE e.userId = :userId", ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
        return listProjects;
    }

    @NotNull
    @Override
    public List<Project> findAllByUserIdEntity(@NotNull final String userId) {
        List<Project> listProjects = entityManager.createQuery("SELECT e FROM Project e WHERE e.user.id = :userId", Project.class)
                .setParameter("userId", userId)
                .getResultList();
        return listProjects;
    }

    @Override
    public void clearByUserId(@NotNull final String userId) {
        @NotNull final List<Project> listProjects = findAllByUserIdEntity(userId);
        for (@NotNull final Project project : listProjects) {
            entityManager.remove(project);
        }
    }

    @Nullable
    @Override
    public ProjectDTO findOneDTOById(@NotNull final String userId, @NotNull final String id) {
        List<ProjectDTO> listProjects = entityManager.createQuery("SELECT e FROM ProjectDTO e WHERE e.userId = :userId AND e.id = :id", ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList();
        if (listProjects.isEmpty()) return null;
        return listProjects.get(0);
    }

    @Nullable
    @Override
    public Project findOneEntityById(@NotNull final String userId, @NotNull final String id) {
        List<Project> listProjects = entityManager.createQuery("SELECT e FROM Project e WHERE e.user.id = :userId AND e.id = :id", Project.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList();
        if (listProjects.isEmpty()) return null;
        return listProjects.get(0);
    }

    @Nullable
    @Override
    public ProjectDTO findOneDTOByIndex(@NotNull final String userId, @NotNull final Integer index) {
        List<ProjectDTO> listProjects = entityManager.createQuery("SELECT e FROM ProjectDTO e WHERE e.userId = :userId", ProjectDTO.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getResultList();
        if (listProjects.size() < index) return null;
        return listProjects.get(index);
    }

    @Nullable
    @Override
    public Project findOneEntityByIndex(@NotNull final String userId, @NotNull final Integer index) {
        List<Project> listProjects = entityManager.createQuery("SELECT e FROM Project e WHERE e.user.id = :userId", Project.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getResultList();
        if (listProjects.size() < index) return null;
        return listProjects.get(index);
    }

    @Nullable
    @Override
    public ProjectDTO findOneDTOByName(@NotNull final String userId, @NotNull final String name) {
        List<ProjectDTO> listProjects = entityManager.createQuery("SELECT e FROM ProjectDTO e WHERE e.userId = :userId AND e.name = :name", ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1)
                .getResultList();
        if (listProjects.isEmpty()) return null;
        return listProjects.get(0);
    }

    @Nullable
    @Override
    public Project findOneEntityByName(@NotNull final String userId, @NotNull final String name) {
        List<Project> listProjects = entityManager.createQuery("SELECT e FROM Project e WHERE e.user.id = :userId AND e.name = :name", Project.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1)
                .getResultList();
        if (listProjects.isEmpty()) return null;
        return listProjects.get(0);
    }

    @Override
    public void removeOneById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Project project = findOneEntityById(userId, id);
        if (project == null) return;
        entityManager.remove(project);
    }

    @Override
    public void removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final Project project = findOneEntityByIndex(userId, index);
        if (project == null) return;
        entityManager.remove(project);
    }

    @Override
    public void removeOneByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Project project = findOneEntityByName(userId, name);
        if (project == null) return;
        entityManager.remove(project);
    }

    @Override
    public void clearAll() {
        @NotNull final List<Project> listProjects = getListEntity();
        for (@NotNull final Project project : listProjects) {
            entityManager.remove(project);
        }
    }

}
