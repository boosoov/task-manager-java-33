package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.service.IUserService;
import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectHashPasswordException;
import com.rencredit.jschool.boruak.taskmanager.repository.UserRepository;
import com.rencredit.jschool.boruak.taskmanager.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserService extends AbstractService<UserDTO> implements IUserService {

    @Autowired
    private UserRepository repository;

    @Override
    @Transactional
    public void add(@Nullable final String login, @Nullable final String password) throws DeniedAccessException, EmptyLoginException, EmptyUserException, BusyLoginException, EmptyHashLineException, EmptyPasswordException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();

        @Nullable final String passwordHash = HashUtil.getHashLine(password);
        if (passwordHash == null) throw new DeniedAccessException();
        @NotNull final UserDTO user = new UserDTO(login, passwordHash);
        add(login, user);
    }

    @Override
    @Transactional
    public void add(@Nullable final String login, @Nullable final String password, @Nullable final String firstName) throws DeniedAccessException, EmptyLoginException, EmptyUserException, BusyLoginException, EmptyHashLineException, EmptyFirstNameException, EmptyPasswordException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();

        @Nullable final String passwordHash = HashUtil.getHashLine(password);
        if (passwordHash == null) throw new DeniedAccessException();
        @NotNull final UserDTO user = new UserDTO(login, passwordHash, firstName);
        add(login, user);
    }

    @Override
    @Transactional
    public void add(@Nullable final String login, @Nullable final String password, @Nullable final Role role) throws DeniedAccessException, EmptyLoginException, EmptyUserException, BusyLoginException, EmptyHashLineException, EmptyRoleException, EmptyPasswordException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();

        @Nullable final String passwordHash = HashUtil.getHashLine(password);
        if (passwordHash == null) throw new DeniedAccessException();
        @NotNull final UserDTO user = new UserDTO(login, passwordHash, role);
        add(login, user);
    }

    @Transactional
    public void add(@Nullable final String login, @Nullable final UserDTO user) throws BusyLoginException, EmptyUserException, EmptyLoginException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (user == null) throw new EmptyUserException();

        if (repository.findByLoginDTO(login) != null) throw new BusyLoginException();
        repository.add(user);
    }


    @Override
    @Transactional
    public void addUser(@Nullable final String login, @Nullable final String password) throws EmptyLoginException, EmptyPasswordException, EmptyHashLineException, DeniedAccessException, EmptyUserException, BusyLoginException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();

        @Nullable final String passwordHash = HashUtil.getHashLine(password);
        if (passwordHash == null) throw new DeniedAccessException();
        @NotNull final User user = new User(login, passwordHash);
        addUser(login, user);
    }

    @Override
    @Transactional
    public void addUser(@Nullable final String login, @Nullable final String password, @Nullable final Role role) throws EmptyLoginException, EmptyPasswordException, EmptyRoleException, EmptyHashLineException, DeniedAccessException, BusyLoginException, EmptyUserException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();

        @Nullable final String passwordHash = HashUtil.getHashLine(password);
        if (passwordHash == null) throw new DeniedAccessException();
        @NotNull final User user = new User(login, passwordHash, role);
        addUser(login, user);
    }

    @Override
    @Transactional
    public void addUser(@Nullable final String login, @Nullable final User user) throws EmptyLoginException, EmptyUserException, BusyLoginException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (user == null) throw new EmptyUserException();

        if (repository.findByLoginDTO(login) != null) throw new BusyLoginException();
    }

    @Nullable
    @Override
    public UserDTO getById(@Nullable final String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        @Nullable final UserDTO user = repository.findByIdDTO(id);
        return user;
    }

    @Nullable
    @Override
    public UserDTO getByLogin(@Nullable final String login) throws EmptyLoginException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return repository.findByLoginDTO(login);
    }

    @Nullable
    @Override
    @Transactional
    public UserDTO updatePasswordById(@Nullable final String id, @Nullable final String newPassword) throws EmptyUserException, IncorrectHashPasswordException, EmptyHashLineException, EmptyNewPasswordException, EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (newPassword == null || newPassword.isEmpty()) throw new EmptyNewPasswordException();

        @Nullable final String passwordHash = HashUtil.getHashLine(newPassword);
        if (passwordHash == null) throw new IncorrectHashPasswordException();
        @Nullable final UserDTO user = repository.findByIdDTO(id);
        if (user == null) throw new EmptyUserException();
        user.setPasswordHash(passwordHash);
        repository.merge(user);
        return user;
    }

    @Nullable
    @Override
    @Transactional
    public UserDTO editProfileById(@Nullable final String id, @Nullable final String firstName) throws EmptyUserException, EmptyFirstNameException, EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();

        @Nullable final UserDTO user = repository.findByIdDTO(id);
        if (user == null) throw new EmptyUserException();
        user.setFirstName(firstName);
        repository.merge(user);
        return user;

    }

    @Nullable
    @Override
    @Transactional
    public UserDTO editProfileById(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName
    ) throws EmptyUserException, EmptyLastNameException, EmptyFirstNameException, EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();
        if (lastName == null || lastName.isEmpty()) throw new EmptyLastNameException();

        @Nullable final UserDTO user = repository.findByIdDTO(id);
        if (user == null) throw new EmptyUserException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        repository.merge(user);
        return user;
    }

    @Nullable
    @Override
    @Transactional
    public UserDTO editProfileById(
            @Nullable final String id,
            @Nullable final String email,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) throws EmptyUserException, EmptyMiddleNameException, EmptyLastNameException, EmptyFirstNameException, EmptyEmailException, EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();
        if (lastName == null || lastName.isEmpty()) throw new EmptyLastNameException();
        if (middleName == null || middleName.isEmpty()) throw new EmptyMiddleNameException();

        @Nullable final UserDTO user = repository.findByIdDTO(id);
        if (user == null) throw new EmptyUserException();
        user.setEmail(email);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        repository.merge(user);
        return user;
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        repository.removeById(id);
    }

    @Override
    @Transactional
    public void removeByLogin(@Nullable final String login) throws EmptyLoginException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();

        repository.removeByLogin(login);
    }

    @Override
    @Transactional
    public void removeByUser(@Nullable final UserDTO user) throws EmptyUserException {
        if (user == null) throw new EmptyUserException();

        repository.removeByUser(user);
    }

    @Nullable
    @Override
    @Transactional
    public UserDTO lockUserByLogin(@Nullable final String login) throws EmptyUserException, EmptyLoginException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();

        @Nullable final UserDTO user = repository.findByLoginDTO(login);
        if (user == null) throw new EmptyUserException();
        user.setLocked(true);
        repository.merge(user);
        return user;
    }

    @Nullable
    @Override
    @Transactional
    public UserDTO unlockUserByLogin(@Nullable final String login) throws EmptyUserException, EmptyLoginException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();

        @Nullable final UserDTO user = repository.findByLoginDTO(login);
        if (user == null) throw new EmptyUserException();
        user.setLocked(false);
        repository.merge(user);
        return user;
    }

    @NotNull
    @Override
    public List<UserDTO> getList() {
        @NotNull final List<UserDTO> list = repository.getListDTO();
        return list;
    }

    @Override
    @Transactional
    public void clearAll() throws EmptyPasswordException, EmptyUserException, EmptyLoginException, DeniedAccessException, BusyLoginException, EmptyHashLineException, EmptyRoleException {
        repository.clearAll();
        add("admin", "admin", Role.ADMIN);
        add("test", "test");
        add("1", "1");
    }

}
