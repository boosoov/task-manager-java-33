package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.repository.IRepository;
import com.rencredit.jschool.boruak.taskmanager.api.service.IService;
import com.rencredit.jschool.boruak.taskmanager.dto.AbstractEntityDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.notexist.NotExistAbstractListException;
import com.rencredit.jschool.boruak.taskmanager.repository.AbstractRepository;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.PersistenceContext;
import java.util.Collection;

public class AbstractService<E extends AbstractEntityDTO> implements IService<E> {

    private AbstractRepository repository;

    @Override
    @Transactional
    public void load(@Nullable final Collection<E> elements) throws EmptyElementsException {
        if (elements == null || elements.isEmpty()) throw new EmptyElementsException();
        repository.load(elements);
    }

    @Override
    @Transactional
    public void load(@Nullable final E... elements) throws EmptyElementsException {
        if (elements == null || elements.length == 0) throw new EmptyElementsException();
        repository.load(elements);
    }

    @Override
    @Transactional
    public boolean merge(@Nullable final E element) throws EmptyElementsException {
        if (element == null) throw new EmptyElementsException();
        repository.merge(element);
        return true;
    }

    @Override
    @Transactional
    public void merge(@Nullable final Collection<E> elements) throws NotExistAbstractListException {
        if (elements == null) throw new NotExistAbstractListException();
        repository.merge(elements);
    }

    @Override
    @Transactional
    public void merge(@Nullable final E... elements) throws NotExistAbstractListException {
        if (elements == null || elements.length == 0) throw new NotExistAbstractListException();
        repository.merge(elements);
    }

    @Override
    @Transactional
    public void clearAll() throws EmptyPasswordException, BusyLoginException, EmptyHashLineException, EmptyLoginException, EmptyUserException, DeniedAccessException, EmptyRoleException {
        repository.clearAll();
    }

}
