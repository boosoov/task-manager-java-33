package com.rencredit.jschool.boruak.taskmanager.repository;

import com.rencredit.jschool.boruak.taskmanager.api.repository.ITaskRepository;
import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class TaskRepository extends AbstractRepository<TaskDTO> implements ITaskRepository {

    @NotNull
    @Override
    public List<TaskDTO> getListDTO() {
        return entityManager.createQuery("SELECT e FROM TaskDTO e", TaskDTO.class).getResultList();
    }

    @NotNull
    @Override
    public List<Task> getListEntity() {
        return entityManager.createQuery("SELECT e FROM Task e", Task.class).getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByUserIdDTO(@NotNull final String userId) {
        @NotNull final List<TaskDTO> listTasks = entityManager.createQuery("SELECT e FROM TaskDTO e WHERE e.userId = :userId", TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
        return listTasks;
    }

    @NotNull
    @Override
    public List<Task> findAllByUserIdEntity(@NotNull final String userId) {
        @NotNull final List<Task> listTasks = entityManager.createQuery("SELECT e FROM Task e WHERE e.user.id = :userId", Task.class)
                .setParameter("userId", userId)
                .getResultList();
        return listTasks;
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllDTOByProjectId(@NotNull final String projectId) {
        @NotNull final List<TaskDTO> listTasks = entityManager.createQuery("SELECT e FROM TaskDTO e WHERE e.projectId = :projectId", TaskDTO.class)
                .setParameter("projectId", projectId)
                .getResultList();
        return listTasks;
    }

    @Override
    public void clearByUserId(@NotNull final String userId) {
        @NotNull final List<Task> listTasks = findAllByUserIdEntity(userId);
        for (@NotNull final Task task : listTasks) {
            entityManager.remove(task);
        }
    }

    @Nullable
    @Override
    public TaskDTO findOneDTOById(@NotNull final String userId, @NotNull final String id) {
        List<TaskDTO> listTasks = entityManager.createQuery("SELECT e FROM TaskDTO e WHERE e.userId = :userId AND e.id = :id", TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList();
        if (listTasks.isEmpty()) return null;
        return listTasks.get(0);
    }

    @Nullable
    @Override
    public Task findOneEntityById(@NotNull final String userId, @NotNull final String id) {
        List<Task> listTasks = entityManager.createQuery("SELECT e FROM Task e WHERE e.user.id = :userId AND e.id = :id", Task.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList();
        if (listTasks.isEmpty()) return null;
        return listTasks.get(0);
    }

    @Nullable
    @Override
    public TaskDTO findOneDTOByIndex(@NotNull final String userId, @NotNull final Integer index) {
        List<TaskDTO> listTasks = entityManager.createQuery("SELECT e FROM TaskDTO e WHERE e.userId = :userId", TaskDTO.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getResultList();
        if (listTasks.size() < index) return null;
        return listTasks.get(index);
    }

    @Nullable
    @Override
    public Task findOneEntityByIndex(@NotNull final String userId, @NotNull final Integer index) {
        List<Task> listTasks = entityManager.createQuery("SELECT e FROM Task e WHERE e.user.id = :userId", Task.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getResultList();
        if (listTasks.size() < index) return null;
        return listTasks.get(index);
    }

    @Nullable
    @Override
    public TaskDTO findOneDTOByName(@NotNull final String userId, @NotNull final String name) {
        List<TaskDTO> listTasks = entityManager.createQuery("SELECT e FROM TaskDTO e WHERE e.userId = :userId AND e.name = :name", TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1)
                .getResultList();
        if (listTasks.isEmpty()) return null;
        return listTasks.get(0);
    }

    @Nullable
    @Override
    public Task findOneEntityByName(@NotNull final String userId, @NotNull final String name) {
        List<Task> listTasks = entityManager.createQuery("SELECT e FROM Task e WHERE e.user.id = :userId AND e.name = :name", Task.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1)
                .getResultList();
        if (listTasks.isEmpty()) return null;
        return listTasks.get(0);
    }

    @Override
    public void removeOneById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Task task = findOneEntityById(userId, id);
        if (task == null) return;
        entityManager.remove(task);
    }

    @Override
    public void removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final Task task = findOneEntityByIndex(userId, index);
        if (task == null) return;
        entityManager.remove(task);

    }

    @Override
    public void removeOneByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Task task = findOneEntityByName(userId, name);
        if (task == null) return;
        entityManager.remove(task);
    }

    @Override
    public void clearAll() {
        @NotNull final List<Task> listTasks = getListEntity();
        for (@NotNull final Task task : listTasks) {
            entityManager.remove(task);
        }
    }

}
