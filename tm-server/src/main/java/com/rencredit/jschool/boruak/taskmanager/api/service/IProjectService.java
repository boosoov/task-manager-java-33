package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectIndexException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectService extends IService<ProjectDTO> {

    void create(@Nullable String userId, @Nullable String name) throws EmptyNameException, EmptyUserIdException;

    void create(@Nullable String userId, @Nullable String name, @Nullable String description) throws EmptyDescriptionException, EmptyNameException, EmptyUserIdException;

    void create(@Nullable final String userId, @Nullable ProjectDTO project) throws EmptyProjectException, EmptyUserIdException;

    void create(@Nullable final String userId, @Nullable Project project) throws EmptyProjectException, EmptyUserIdException;

    void remove(@Nullable String userId, @Nullable ProjectDTO project) throws EmptyProjectException, EmptyUserIdException;

    @NotNull
    List<ProjectDTO> findAllByUserId(@Nullable String userId) throws EmptyUserIdException;

    void clearByUserId(@Nullable String userId) throws EmptyUserIdException;

    @Nullable
    ProjectDTO findOneDTOById(@Nullable String userId, @Nullable String id) throws EmptyIdException, EmptyUserIdException;

    @Nullable
    Project findOneEntityById(@Nullable final String userId, @Nullable final String id) throws EmptyIdException, EmptyUserIdException;

    @Nullable
    ProjectDTO findOneByIndex(@Nullable String userId, @Nullable Integer index) throws IncorrectIndexException, EmptyUserIdException;

    @Nullable
    ProjectDTO findOneByName(@Nullable String userId, @Nullable String name) throws EmptyNameException, EmptyUserIdException;

    @Nullable
    void removeOneById(@Nullable String userId, @Nullable String id) throws EmptyIdException, EmptyUserIdException;

    @Nullable
    void removeOneByIndex(@Nullable String userId, @Nullable Integer index) throws IncorrectIndexException, EmptyUserIdException;

    @Nullable
    void removeOneByName(@Nullable String userId, @Nullable String name) throws EmptyNameException, EmptyUserIdException;

    @NotNull
    ProjectDTO updateProjectById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description) throws EmptyProjectException, EmptyNameException, EmptyIdException, EmptyUserIdException, EmptyDescriptionException;

    @NotNull
    ProjectDTO updateProjectByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description) throws EmptyProjectException, EmptyNameException, IncorrectIndexException, EmptyUserIdException, EmptyDescriptionException;

    @NotNull List<ProjectDTO> getList();

    void clearAll();
}
