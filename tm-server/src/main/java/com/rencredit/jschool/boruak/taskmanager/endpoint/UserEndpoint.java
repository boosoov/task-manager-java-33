package com.rencredit.jschool.boruak.taskmanager.endpoint;

import com.rencredit.jschool.boruak.taskmanager.api.endpoint.IUserEndpoint;
import com.rencredit.jschool.boruak.taskmanager.api.service.IUserService;
import com.rencredit.jschool.boruak.taskmanager.dto.SessionDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectHashPasswordException;
import com.rencredit.jschool.boruak.taskmanager.exception.notexist.NotExistUserException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@Controller
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @Nullable
    @Autowired
    private IUserService userService;

    public UserEndpoint() {
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @WebMethod
    public void addUserLoginPassword(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "login", partName = "login") String login,
            @Nullable @WebParam(name = "password", partName = "password") String password
    ) throws EmptyPasswordException, BusyLoginException, EmptyHashLineException, EmptyLoginException, EmptyUserException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException {
        sessionService.validate(session);
        if (!authService.checkRoles(session, roles())) throw new DeniedAccessException();
        userService.add(login, password);
    }

    @Override
    @WebMethod
    public void addUserLoginPasswordFirstName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "login", partName = "login") String login,
            @Nullable @WebParam(name = "password", partName = "password") String password,
            @Nullable @WebParam(name = "firstName", partName = "firstName") String firstName
    ) throws EmptyPasswordException, EmptyFirstNameException, BusyLoginException, EmptyHashLineException, EmptyLoginException, EmptyUserException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException {
        sessionService.validate(session);
        if (!authService.checkRoles(session, roles())) throw new DeniedAccessException();
        userService.add(login, password, firstName);
    }

    @Override
    @WebMethod
    public void addUserLoginPasswordRole(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "login", partName = "login") String login,
            @Nullable @WebParam(name = "password", partName = "password") String password,
            @Nullable @WebParam(name = "role", partName = "role") Role role
    ) throws EmptyPasswordException, EmptyRoleException, BusyLoginException, EmptyHashLineException, EmptyLoginException, EmptyUserException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptySessionException {
        sessionService.validate(session);
        if (!authService.checkRoles(session, roles())) throw new DeniedAccessException();
        userService.add(login, password, role);
    }

    @Nullable
    @Override
    @WebMethod
    public UserDTO getUserById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) throws EmptyIdException, DeniedAccessException, NotExistUserException, EmptyUserIdException, EmptyRoleException, EmptySessionException {
        sessionService.validate(session);
        if (!authService.checkRoles(session, roles())) throw new DeniedAccessException();
        return userService.getById(id);
    }

    @Nullable
    @Override
    @WebMethod
    public UserDTO getUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) throws EmptyLoginException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException {
        sessionService.validate(session);
        if (!authService.checkRoles(session, roles())) throw new DeniedAccessException();
        return userService.getByLogin(login);
    }

    @Nullable
    @Override
    @WebMethod
    public UserDTO editUserProfileByIdFirstName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "firstName", partName = "firstName") String firstName
    ) throws EmptyIdException, EmptyUserException, EmptyFirstNameException, DeniedAccessException {
        sessionService.validate(session);
        return userService.editProfileById(id, firstName);
    }

    @Nullable
    @Override
    @WebMethod
    public UserDTO editUserProfileByIdFirstNameLastName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "firstName", partName = "firstName") String firstName,
            @Nullable @WebParam(name = "lastName", partName = "lastName") String lastName
    ) throws EmptyUserException, EmptyIdException, EmptyFirstNameException, EmptyLastNameException, DeniedAccessException {
        sessionService.validate(session);
        return userService.editProfileById(id, firstName, lastName);
    }

    @Nullable
    @Override
    @WebMethod
    public UserDTO editUserProfileByIdFirstNameLastNameMiddleName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "email", partName = "email") String email,
            @Nullable @WebParam(name = "firstName", partName = "firstName") String firstName,
            @Nullable @WebParam(name = "lastName", partName = "lastName") String lastName,
            @Nullable @WebParam(name = "middleName", partName = "middleName") String middleName
    ) throws EmptyIdException, EmptyLastNameException, EmptyEmailException, EmptyMiddleNameException, EmptyFirstNameException, EmptyUserException, DeniedAccessException {
        sessionService.validate(session);
        return userService.editProfileById(id, email, firstName, lastName, middleName);
    }

    @Nullable
    @Override
    @WebMethod
    public UserDTO updateUserPasswordById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "newPassword", partName = "newPassword") String newPassword
    ) throws EmptyUserException, IncorrectHashPasswordException, EmptyIdException, EmptyNewPasswordException, EmptyHashLineException, DeniedAccessException {
        sessionService.validate(session);
        return userService.updatePasswordById(id, newPassword);
    }

    @Override
    @WebMethod
    public void removeUserById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) throws EmptyIdException, DeniedAccessException, NotExistUserException, EmptyUserIdException, EmptyRoleException, EmptySessionException {
        sessionService.validate(session);
        if (!authService.checkRoles(session, roles())) throw new DeniedAccessException();
        userService.removeById(id);
    }

    @Override
    @WebMethod
    public void removeUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) throws EmptyLoginException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException {
        sessionService.validate(session);
        if (!authService.checkRoles(session, roles())) throw new DeniedAccessException();
        userService.removeByLogin(login);
    }

    @Override
    @WebMethod
    public void clearAllUser(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException, EmptyUserException, EmptyLoginException, EmptyPasswordException, BusyLoginException, EmptyHashLineException {
        sessionService.validate(session);
        if (!authService.checkRoles(session, roles())) throw new DeniedAccessException();
        userService.clearAll();
    }

    @NotNull
    @Override
    @WebMethod
    public List<UserDTO> getUserList(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws DeniedAccessException, EmptyIdException, NotExistUserException, EmptyUserIdException, EmptyRoleException, EmptySessionException {
        sessionService.validate(session);
        if (!authService.checkRoles(session, roles())) throw new DeniedAccessException();
        return userService.getList();
    }

}
